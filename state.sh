#!/bin/bash
set -e

out=$(aws ec2 describe-instance-status --instance-id i-0bf07053a97b31c9e)
echo "$out"
output=$(echo "$out" | jq -r '.InstanceStatuses')
Status=$(echo "$out" | jq -r '.InstanceStatuses[].InstanceState["Name"]')
if [ $output=[] ];
        then
        aws ec2 start-instances --instance-ids i-0bf07053a97b31c9e

elif [ $Status="running"  ]
        then
        aws ec2 stop-instances --instance-ids i-0bf07053a97b31c9e
fi